# chatWithDocuments
 
## Project Overview
 
### Client (Frontend)
 
The client/ directory contains the React frontend code.
 
### Server (Backend)
 
The server/ directory contains the Django backend code.
 
## Getting Started
 
To run the project locally, follow these steps:
 
1. Clone the repository:
 
   ```bash
   git clone https://gitlab.com/genai-poc/file-convo-ai-p2.git
   cd file-convo-ai-p2
   ```
 
2. Install dependencies for the client:
 
   ```bash
   >cd client
   >npm install
   ```
 
3. Run the client:
 
   ```bash
   >npm start
   ```
 
4. Create and activate a virtual environment:

   Note: use command prompt to run the commands below.
 
   ```bash
   >cd ../server
   >python -m venv venv
   >cd venv/Scripts
   >.\activate
   ```
 
5. Install dependencies for the server:
 
   ```bash
   (venv) >cd ../server
   (venv) >pip install -r requirements.txt
   ```
 
   requirements.txt file contains all the packages required for the backend to function properly.
 
6. Install SQLite DB and add the PATH to the environment.

7. Run the server:

   Note: Generate your own OPENAI API Key and add it to a .env file in the server/ directory
 
   ```bash
   (venv) >cd server/backend
   (venv) >python manage.py runserver
   ```
 
7. Open your browser and visit [http://localhost:3000](http://localhost:3000) to view the application.
 
## Technologies Used
 
### Frontend:
 
- React
 
### Backend:
 
- Django REST Framework
 
### Database:
 
- SQLite
